#!/bin/sh

is_command() {
  command -v "$1" >/dev/null 2>&1;
}

missing() {
  is_command "$1" && return 1 || return 0;
}

if [ -z "$CI_JOB_TOKEN" ]; then
  echo "'\$CI_JOB_TOKEN' must be set.  Aborting."
  exit 1
fi

if is_command "apk"; then
  echo "Ensuring that 'curl' is installed"
  apk add --no-cache libcurl
elif is_command "apt-get"; then
  echo "Ensuring that 'curl' is installed"
  sudo apt-get install -y libcurl4-openssl-dev
fi

if missing "git"; then
  echo "Didn't find 'git'.  Attempting to install."
  if is_command "apk"; then
    echo "Installing 'git' with 'apk'."
    apk add --no-cache git
  elif is_command "apt-get"; then
    echo "Installing 'git' with 'apt-get'."
    apt-get install git
  else
    echo "Couldn't install 'git'.  Aborting."
    exit 2
  fi
fi

NEW_URL="https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com"

git config --add --global url."$NEW_URL/".insteadOf "git@gitlab.com:"
git config --add --global url."$NEW_URL".insteadOf "ssh://git@gitlab.com"

git config --add --global url."$NEW_URL".insteadOf "git://git@gitlab.com"
git config --add --global url."$NEW_URL".insteadOf "git+ssh://git@gitlab.com"

echo "Configured CI to clone private repositories."
