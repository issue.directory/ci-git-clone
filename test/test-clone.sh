set -e

git clone git@gitlab.com:issue.directory/sample-private-project.git git
rm -rf git

git clone git+ssh://git@gitlab.com/issue.directory/sample-private-project.git git+ssh
rm -rf git+ssh

git clone ssh://git@gitlab.com/issue.directory/sample-private-project.git ssh
rm -rf ssh
