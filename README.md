# GitLab CI Private Repository Clone

Configures Git to clone private repositories using `$GITLAB_CI_TOKEN`.
Will install `git` if not already installed on the machine.

## Usage

```yaml
before_script:
  - curl https://gitlab.com/issue.directory/ci-git-clone/raw/master/clone.sh | sh
```
